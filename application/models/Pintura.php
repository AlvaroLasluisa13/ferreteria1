<?php

  class Pintura extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }


    function insertar($datos){
        //Active record en CodeIgniter
        return $this->db->insert('pintura',$datos);
    }

    function obtenerTodos(){
        $listadoPinturas=$this->db->get("pintura");
        if ($listadoPinturas->num_rows()>0) {

          return $listadoPinturas->result();
        } else {
          return false;
        }
    }
    //borrar Pinturas
    function borrar($id_pin){
      $this->db->where("id_pin",$id_pin);
        return $this->db->delete("pintura");
    }
  }//cierre de la clase
 ?>
