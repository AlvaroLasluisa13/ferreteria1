<?php

  class Electrico extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }


    function insertar($datos){
        //Active record en CodeIgniter
        return $this->db->insert('electrico',$datos);
    }

    function obtenerTodos(){
        $listadoElectricos=$this->db->get("electrico");
        if ($listadoElectricos->num_rows()>0) {

          return $listadoElectricos->result();
        } else {
          return false;
        }
    }
    //borrar Electricos
    function borrar($id_ele){
      $this->db->where("id_ele",$id_ele);
        return $this->db->delete("electrico");
    }
  }//cierre de la clase
 ?>
