<?php

  class Industrial extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }


    function insertar($datos){
        //Active record en CodeIgniter
        return $this->db->insert('industrial',$datos);
    }

    function obtenerTodos(){
        $listadoIndustriales=$this->db->get("industrial");
        if ($listadoIndustriales->num_rows()>0) {

          return $listadoIndustriales->result();
        } else {
          return false;
        }
    }
    //borrar Industriales
    function borrar($id_ind){
      $this->db->where("id_ind",$id_ind);
        return $this->db->delete("industrial");
    }
  }//cierre de la clase
 ?>
