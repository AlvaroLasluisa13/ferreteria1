<div class="row">
  <div class="col-md-8">
    <h1>LISTADO DE MATERIAL INDUSTRIAL</h1>
    <br>
  </div>
  <div class="col-md-4">
    <a href="<?php echo site_url('electricos/nuevo')?>" class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>
      Agregar Materiales
    </a>
  </div>
</div>

<?php if ($industriales): ?>
   <table class="table table-striped table-bordered table-hover">
     <thead>
       <tr>
         <th>ID</th>
         <th> NOMBRE:</th>
         <th>MARCA:</th>
         <th>PROVEEDOR</th>
         <th>PRECIO</th>
         <th>DESCRIPCIIDON</th>
         <th>ACCIONES</th>
       </tr>
     </thead>
     <tbody>
       <?php foreach ($industriales as $filaTemporal): ?>
       <tr>
         <td><?php echo $filaTemporal->id_ind;?></td>
         <td><?php echo $filaTemporal->nombre_ind;?></td>
         <td><?php echo $filaTemporal->marca_ind;?></td>
         <td><?php echo $filaTemporal->proveedor_ind;?></td>
         <td><?php echo $filaTemporal->precio_ind;?></td>
         <td><?php echo $filaTemporal->descripcion_ind;?></td>
         <td class="text-center">
          <!-- <a href="#" title="editar material"> <i class="glyphicon glyphicon-pencil"> </i> </a>&nbsp;&nbsp;-->&nbsp;&nbsp;
          <a href="<?php echo site_url(); ?>/industrial/eliminar/<?php echo $filaTemporal->id_ind;?>"title="borrar materia" onclick="return confirm ('¿Estas seguro de eliminar?')"> <i class="glyphicon glyphicon-trash" style="color:red;"> </i> </a>
        </td>

       </tr>
     <?php endforeach;?>
          </tbody>

   </table>
<?php else: ?>
  <h1>No hay datos</h1>
<?php endif; ?>
