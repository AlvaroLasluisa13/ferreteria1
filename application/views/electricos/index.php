<div class="row">
  <div class="col-md-8">
    <h1>LISTADO DE MATERIALES ELECTRICOS</h1>
    <br>
  </div>
  <div class="col-md-4">
    <a href="<?php echo site_url('electricos/nuevo')?>" class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>
      Agregar Materiales
    </a>
  </div>
</div>

<?php if ($electricos): ?>
   <table class="table table-striped table-bordered table-hover">
     <thead>
       <tr>
         <th>ID</th>
         <th> NOMBRE:</th>
         <th>MARCA:</th>
         <th>PROVEEDOR</th>
         <th>PRECIO</th>
         <th>DESCRIPCIIDON</th>
         <th>ACCIONES</th>
       </tr>
     </thead>
     <tbody>
       <?php foreach ($electricos as $filaTemporal): ?>
       <tr>
         <td><?php echo $filaTemporal->id_ele;?></td>
         <td><?php echo $filaTemporal->nombre_ele;?></td>
         <td><?php echo $filaTemporal->marca_ele;?></td>
         <td><?php echo $filaTemporal->proveedor_ele;?></td>
         <td><?php echo $filaTemporal->precio_ele;?></td>
         <td><?php echo $filaTemporal->descripcion_ele;?></td>
         <td class="text-center">
           <!--<a href="#" title="editar material"> <i class="glyphicon glyphicon-pencil"> </i> </a>&nbsp;-->&nbsp;&nbsp;&nbsp;
          <a href="<?php echo site_url(); ?>/electricos/eliminar/<?php echo $filaTemporal->id_ele;?>"title="borrar materia" onclick="return confirm ('¿Estas seguro de eliminar?')"> <i class="glyphicon glyphicon-trash" style="color:red;"> </i> </a>
        </td>
       </tr>
     <?php endforeach;?>
          </tbody>

   </table>
<?php else: ?>
  <h1>No hay datos</h1>
<?php endif; ?>
