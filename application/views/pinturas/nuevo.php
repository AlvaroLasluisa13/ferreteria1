<h1>Nuevas pinturas</h1>

<form class="" action="<?php echo site_url(); ?>/pinturas/guardar" method="post">

  <div class="row">
    <div class="col-md-4">

       <label for="">nombre:</label>
       <br>
       <input type="text" class="form-control" name="nombre_pin" value="" id="nombre_pin" placeholder="Ingrese nombre">

    </div>
    <div class="col-md-5">

      <label for="">marca:</label>
      <br>
      <input type="text" class="form-control"name="marca_pin" value="" id="marca_pin " placeholder="Ingrese su marca">

    </div>
    <div class="col-md-3">
      <label for="">Proveedor:</label>
      <br>
      <input type="text" class="form-control"name="proveedor_pin" id="proveedor_pin" value=""  placeholder="Ingrese su proveedor">
    </div>

  </div>

  <div class="row">

    <div class="col-md-4">
      <label for="">precio</label>
      <br>
      <input type="number" class="form-control"name="precio_pin" value="" id="precio_pin" placeholder="Ingrese su  precio">


    </div>
    <div class="col-md-5">
      <label for="">descripcion:</label>
      <br>
      <input type="text" class="form-control"name="descripcion_pin" value=""id="descripcion_pin" placeholder="Ingrese su descripcion">
    </div>
  </div>


  <br> <br><br>
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
      <a href="<?php echo site_url(); ?>/pinturas/index" class="btn btn-danger">Cancelar </a>
    </div>

  </div>

</form>
