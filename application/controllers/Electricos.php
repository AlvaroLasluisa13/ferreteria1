<?php
 class Electricos extends CI_Controller
 {

   function __construct()
   {
     parent::__construct();
     //Cargar el modelo
     $this->load->model('Electrico');
   }

   public function index()
   {
     $data['electricos']=$this->Electrico->obtenerTodos();
     $this->load->view('header');
     $this->load->view('electricos/index',$data);
     $this->load->view('footer');
   }

   public function nuevo()
   {
     $this->load->view('header');
     $this->load->view('electricos/nuevo');
     $this->load->view('footer');
   }

   public function guardar(){
     $datosNuevoElectrico= array(
       "nombre_ele"=>$this->input->post('nombre_ele'),
       "marca_ele"=>$this->input->post('marca_ele'),
       "proveedor_ele"=>$this->input->post('proveedor_ele'),
       "precio_ele"=>$this->input->post('precio_ele'),
       "descripcion_ele"=>$this->input->post('descripcion_ele'),
      );
      //$this->Instructor->insertar($datosNuevo);
      if ($this->Electrico->insertar($datosNuevoElectrico))
      {
        redirect ('electricos/index');
      }else {
        echo "<h1>Error al insertar</h1>";
      }

   }
   //funcion para terminar electricos
   public function eliminar($id_ele){
     if ($this->Electrico->borrar($id_ele)) {
       redirect('electricos/index');
     }else {
       echo "ERROR AL BORRAR :(";
     }

   }
 }

 ?>
