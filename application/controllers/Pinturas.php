<?php
 class Pinturas extends CI_Controller
 {

   function __construct()
   {
     parent::__construct();
     //Cargar el modelo
     $this->load->model('Pintura');
   }

   public function index()
   {
     $data['pinturas']=$this->Pintura->obtenerTodos();
     $this->load->view('header');
     $this->load->view('pinturas/index',$data);
     $this->load->view('footer');
   }

   public function nuevo()
   {
     $this->load->view('header');
     $this->load->view('pinturas/nuevo');
     $this->load->view('footer');
   }

   public function guardar(){
     $datosNuevoPintura= array(
       "nombre_pin"=>$this->input->post('nombre_pin'),
       "marca_pin"=>$this->input->post('marca_pin'),
       "proveedor_pin"=>$this->input->post('proveedor_pin'),
       "precio_pin"=>$this->input->post('precio_pin'),
       "descripcion_pin"=>$this->input->post('descripcion_pin'),
      );
      //$this->Instructor->insertar($datosNuevo);
      if ($this->Pintura->insertar($datosNuevoPintura))
      {
        redirect ('pinturas/index');
      }else {
        echo "<h1>Error al insertar</h1>";
      }

   }
   //funcion para terminar pinturas
   public function eliminar($id_pin){
     if ($this->Pintura->borrar($id_pin)) {
       redirect('pinturas/index');
     }else {
       echo "ERROR AL BORRAR :(";
     }

   }
 }

 ?>
