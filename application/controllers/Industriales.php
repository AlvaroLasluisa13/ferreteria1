<?php
 class Industriales extends CI_Controller
 {

   function __construct()
   {
     parent::__construct();
     //Cargar el modelo
     $this->load->model('Industrial');
   }

   public function index()
   {
     $data['industriales']=$this->Industrial->obtenerTodos();
     $this->load->view('header');
     $this->load->view('industriales/index',$data);
     $this->load->view('footer');
   }

   public function nuevo()
   {
     $this->load->view('header');
     $this->load->view('industriales/nuevo');
     $this->load->view('footer');
   }

   public function guardar(){
     $datosNuevoIndustrial= array(
       "nombre_ind"=>$this->input->post('nombre_ind'),
       "marca_ind"=>$this->input->post('marca_ind'),
       "proveedor_ind"=>$this->input->post('proveedor_ind'),
       "precio_ind"=>$this->input->post('precio_ind'),
       "descripcion_ind"=>$this->input->post('descripcion_ind'),
      );
      //$this->Instructor->insertar($datosNuevo);
      if ($this->Industrial->insertar($datosNuevoIndustrial))
      {
        redirect ('industriales/index');
      }else {
        echo "<h1>Error al insertar</h1>";
      }

   }
   //funcion para terminar Industriales
   public function eliminar($id_ind){
     if ($this->Industrial->borrar($id_ind)) {
       redirect('industriales/index');
     }else {
       echo "ERROR AL BORRAR :(";
     }

   }
 }

 ?>
